@extends('layouts.dashboard')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @if(Auth::user()->verified )
                <div class="panel-heading">Meine Job Inserate</div>
                <div class="panel-body">
                    <ul class="list-group">
                        @if(!empty(Auth::user()->posts))
                        @foreach( Auth::user()->posts as $postItem )
                        <li class="list-group-item">
                            {{$postItem->jobTitle}}
                            <a href="/dashboard/{{$postItem->id}}" data-toggle="modal" style="float:right; margin-top:-6px; margin-right:-10px;">
                                <button class="btn btn-primary">Bearbeiten</button>
                            </a>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                </div>
            @else
            User ist noch nicht verifiziert.
            @endif
        </div>
    </div>
</div>
</div>
@endsection