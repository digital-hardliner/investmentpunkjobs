<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		return view('account');
	}

	public function update(Request $request, User $user) {
		if (!empty($request->logo)) {
			$logo = Hash::make($request->name);
			$path = $request->logo->storeAs('public/uploadedlogos', $logo);
			$data['logo']->move(base_path() . '/public/', $logo);

		}
		$user->update($request->all());
		if (!empty($request->logo)) {
			$user->update(["logo" => $logo]);
		}
		return redirect('dashboard');
	}

	public function show(User $user) {
		return view('updateAccount', compact('user'));
	}
}
