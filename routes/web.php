<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
 */
Auth::routes();
Route::get('/', 'landingPageController@index');
Route::get('/dashboard', 'DashboardController@index');
Route::post('/postajob', 'PostController@store');
Route::get('/suchen', 'PostController@search');
Route::delete('/{post}/delete', 'PostController@delete');
Route::patch('/{post}/update', 'PostController@update');
Route::get('/{post}', 'PostController@showJob');
Route::get('/dashboard/{post}', 'PostController@show');
Route::patch('/user/{user}/update', 'DashboardController@update');
Route::get('/account/{user}', 'DashboardController@show');