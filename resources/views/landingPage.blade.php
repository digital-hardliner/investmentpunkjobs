@extends('layouts.master')
@section('body')
<div class="container">

    <ul class="list-group">
        @foreach ($posts as $post)
        <li class="list-group-item">
            <div class="row" style="margin-top:0em;">
                <div class="col-md-12" style="margin-top:0em;">
                    <div class="post">
                        <div class="col-md-4">

                            <img src="storage/uploadedlogos/{{$post->user->logo}}" style="margin-top:4em; width:80%">
                            </img>
                            {{-- <img src="{{$post->user->logo}}.png" style="margin-top:4em; width:80%">
                            </img> --}}
                        </div>
                        <div class="col-md-8">
                            <h3>
                            {{$post->jobTitle}} in {{$post->location}}
                            </h3>
                            <p>
                                {{$post->jobDescription}}
                            </p>
                             <a data-toggle="modal" href="/{{$post->id}}">
                            <button class="btnnew lgnew ghost">
                            Details
                            </button>
                        </a>
                        </div>

                    </div>
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</div>
@stop