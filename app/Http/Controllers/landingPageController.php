<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Post;

class landingPageController extends Controller {
	// public function __construct()
	// {
	//     $this->middleware('auth');
	// }

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$posts = Post::all();

		return view('landingPage', compact('posts'));
	}

}
