<div class="portfolio-modal modal fade" id="updateJob" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="close-modal" data-dismiss="modal">
        <div class="lr">
          <div class="rl">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <div class="modal-body">
              <!-- Project Details Go Here -->
              <h2>  {{$postItem->jobTitle}} Jobinserat bearbeiten</h2>
              <p class="item-intro text-muted">
                <form class="form-horizontal" action="{{$postItem->id}}/update" method="POST">
                  {{method_field('PATCH')}}
                  {{ csrf_field() }}
                  <input type="hidden" class="form-control" name="user_id" value={{Auth::user()->id}}>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Firmenname</label>
                    <input type="text" class="form-control" name="companyName" placeholder="Firmenbezeichnung" value="{{Auth::user()->name}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Job Bezeichnung</label>
                    <input type="text" class="form-control" name="jobTitle" placeholder="Job Bezeichnung" value={{$postItem->jobTitle}}>
                  </div>
                  <div class="form-group">
                    <label for="jobDescription">Job Beschreibung</label>
                    <textarea class="form-control" rows="3" name="jobDescription" placeholder="Job Beschreibung"> {{$postItem->jobDescription}} </textarea>
                  </div>
                  {{-- <div class="form-group">
                    <label for="exampleInputFile">Firmenlogo</label>
                    <input type="file" id="exampleInputFile">
                  </div>
                  --}}
                  <button type="submit" class="btn btn-primary">Job Inserat ändern</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>