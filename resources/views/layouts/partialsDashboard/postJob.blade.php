<div class="portfolio-modal modal fade" id="job-schalten" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="close-modal" data-dismiss="modal">
        <div class="lr">
          <div class="rl">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <div class="modal-body">
              <!-- Project Details Go Here -->
              <h2>Schalten Sie heute noch einen Job</h2>
              <p class="item-intro text-muted">Wir haben einen außerordentlichen Pool an talentierten und brillianten Köpfen.</p>
              <form class="form-horizontal" action="/postajob" method="POST">
                {{ csrf_field() }}
                <input type="hidden" class="form-control" name="user_id" value={{Auth::user()->id}}>
                <div class="form-group">
                  <label for="exampleInputPassword1">Job Bezeichnung</label>
                  <input type="text" class="form-control" name="jobTitle" placeholder="Job Bezeichnung">
                </div>
                <div class="form-group">
                  <label for="jobDescription">Job Beschreibung</label>
                  <textarea class="form-control" rows="10" name="jobDescription" placeholder="Job Beschreibung"></textarea>
                </div>
                <div class="form-group">
                  <label for="location">Ort</label>
                  <input type="text" class="form-control" name="location" placeholder="Ort">
                </div>
                <button type="submit" class="btn btn-primary">Job Schalten</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>