@include("layouts.partialsDashboard.head")
<div class="portfolio-modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="close-modal">
				<a href="/">
					<div class="lr">
						<div class="rl">
						</div>
					</div>
				</a>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="modal-body">
							<h2>
							{{$post->jobTitle}}
							</h2>
							<p>
								<strong>
								Firmenname:
								</strong>
								{{ $post->user->name }}
							</p>
							<p>
								{{-- <img width="400px"src="storage/uploadedlogos/{{$post->user->logo}}" style="margin-top:2em;">
								</img> --}}
								<img width="400px"src="/{{$post->user->logo}}" style="margin-top:2em;">
								</img>
							</p>
							<p>
								<strong>
								Job Beschreibung:
								</strong>
								{{$post->jobDescription}}
							</p>
							<p>
								<strong>
								Ort:
								</strong>
								{{$post->location}}
							</p>
							<div class="panel panel-default" style="">
								<div class="panel-heading">
									Kontakt
								</div>
								<div class="panel-body">
									<strong>
									Adresse:
									</strong>
									{{$post->user->adress}}
									<strong>
									Email:
									</strong>
									{{$post->user->email}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include("layouts.partialsDashboard.foot")