<div class="portfolio-modal modal fade" id="profil" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="close-modal" data-dismiss="modal">
        <div class="lr">
          <div class="rl">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <div class="modal-body">
              <h2>  {{Auth::user()->name}} </h2>
              <form class="form-horizontal" action="/{{Auth::user()->id}}/update" method="POST">
              {{method_field('PATCH')}}
              {{ csrf_field() }}
                <div class="form-group">
                  <label for="name">Firmenname</label>
                  <input type="text" class="form-control" name="name" placeholder="Firmenbezeichnung" value="{{Auth::user()->name}}">
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" class="form-control" name="email" placeholder="Email" value="{{Auth::user()->email}}">
                </div>
                <div class="form-group">
                  <label for="adress">Adresse</label>
                  <input type="text" class="form-control" name="adress" placeholder="Adresse" value="{{Auth::user()->adress}}">
                </div>
                <button type="submit" class="btn btn-primary">Bearbeiten</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>