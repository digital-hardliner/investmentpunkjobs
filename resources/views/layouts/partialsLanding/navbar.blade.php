<nav class="navbar navbar-default" style="position:relative; overflow: auto;">
    <div class="container-fluid" style="margin-right:0%;margin-left:0%;margin-top:15px;">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="ic on-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <!-- Branding Image -->
            <a class="navbar-brand" href="/" style="margin-top:-18px; margin-left:5px; position:relative; z-index:100;"> <img src="punklogo.png" height="50" width="100"> </a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-right">
                @if (Auth::guest())
                <a href="#login" data-toggle="modal"><button class="btnnew smnew ghost">Login</button>
                </a>
                @else
                <a href="/dashboard"><button class="btnnew smnew ghost"> Dashboard </button></a>
                @endif
            </ul>
        </div>
    </div>
</nav>