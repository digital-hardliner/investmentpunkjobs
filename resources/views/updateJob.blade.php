@include("layouts.partialsDashboard.head")
<div class="portfolio-modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<a href="/dashboard">
					<div class="lr">
						<div class="rl" >
						</div>
					</div>
				</div>
			</a>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="modal-body">
							<!-- Project Details Go Here -->
							<h2> Jobinserat bearbeiten</h2>
							<p class="item-intro text-muted">
								<form class="form-horizontal" action="/{{$post->id}}/update" method="POST">
									{{method_field('PATCH')}}
									{{ csrf_field() }}
									<input type="hidden" class="form-control" name="user_id" value={{Auth::user()->id}}>
									<div class="form-group">
										<label for="jobTitle">Job Bezeichnung</label>
										<input type="text" class="form-control" name="jobTitle" placeholder="Job Bezeichnung" value="{{$post->jobTitle}}">
									</div>
									<div class="form-group">
										<label for="jobDescription">Job Beschreibung</label>
										<textarea class="form-control" rows="10" name="jobDescription" placeholder="Job Beschreibung"> {{$post->jobDescription}} </textarea>
									</div>
									<button type="submit" class="btn btn-primary">Bearbeiten</button>

								</form>
								<form class="form-vertical" action="/{{$post->id}}/delete" method="POST">
								{{ method_field('DELETE') }}
  								{{ csrf_field() }}
								<button type="submit"class="btn btn-primary" style="background-color:red;">Löschen</button>
								</form>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include("layouts.partialsDashboard.foot")