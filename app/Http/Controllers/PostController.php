<?php
namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller {
	public function store(Request $request) {
		$postclone = new Post;
		$postclone->jobTitle = $request->jobTitle;
		$postclone->jobDescription = $request->jobDescription;
		$postclone->user_id = $request->user_id;
		$postclone->location = $request->location;
		\DB::table('posts')->insert(['jobTitle' => $postclone->jobTitle, 'jobDescription' => $postclone->jobDescription, 'user_id' => $postclone->user_id, 'location' => $postclone->location]);
		return redirect('/dashboard');
	}

	public function search(Request $request) {
		$posts = Post::all();
		$searchedPosts1 = array();
		$searchedPosts2 = array();

		if (!empty($request->job)) {
			foreach ($posts as $post) {
				if (strpos(strtolower($post->jobTitle), strtolower($request->job)) !== false) {
					$searchedPosts1[] = $post;
				};
			}
		}
		if (!empty($request->location)) {
			foreach ($posts as $post) {
				if (strpos(strtolower($post->location), strtolower($request->location)) !== false) {
					$searchedPosts2[] = $post;
				};
			}
		}

		if (!empty($request->location) && !empty($request->job)) {
			$result = array_intersect($searchedPosts1, $searchedPosts2);
		} elseif (!empty($request->location)) {
			$result = $searchedPosts2;
		} elseif (!empty($request->job)) {
			$result = $searchedPosts1;
		} else {
			\Session::flash('message', 'Bitte Suchbegriffe eingeben.');
			return redirect('/');
		}

		$posts = $result;
		if ($posts !== []) {
			return view('landingPage', compact('posts'));
		} else {
			return view('noResults', compact('request'));
		}

	}

	public function update(Request $request, Post $post) {
		$post->update($request->all());
		return redirect('/dashboard');
	}

	public function show(Post $post) {
		return view('updateJob', compact('post'));
	}

	public function showJob(Post $post) {
		return view('showJob', compact('post'));
	}

	public function delete(Post $post) {
		$post->delete();
		return redirect('/dashboard');
	}

}
