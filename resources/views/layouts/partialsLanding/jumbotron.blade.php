<div class="container-fluid">
  <div class="jumbotron">
    <video id="bg-video" autoplay="true" loop="loop" preload="metadata" muted="muted">
      <source src="jumbotronvideo.mp4" type="video/mp4" />
    </video>
    <div class="col-md-6 col-md-offset-3">
      <div class="center jumbovidtext text-center">
        <h1 class="txtjumbo">Finde einen Job, den du liebst.</h1>
        <form class="navbar-form" role="search" style="" action="/suchen" method="GET">
          <div class="input-group" style="">
            <div class="searchbar">
            <input type="text" class="form-control" placeholder="Jobtitel, Position, Skills..." name="job" style="width:20vw;">
            <input type="text" class="form-control" placeholder="Ort" name="location" style="width:10vw;">
            <div class="input-group-btn">
              <button class="btn btn-default" style="background-color:white;" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
          </div>
          </div>
        </form>
        @if(Session::has('message'))
        <div class="alert alert-danger">
          {{ Session::get('message') }}
        </div>
        @endif
      </div>
    </div>
  </div>
</div>