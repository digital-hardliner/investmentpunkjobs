@include("layouts.partialsDashboard.head")
<div class="portfolio-modal">
  <div class="modal-dialog">
    <div class="modal-content">
    <a href="{{url('/dashboard')}}">
      <div class="close-modal">
        <div class="lr">
          <div class="rl">
          </div>
        </div>
      </div>
      </a>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <div class="modal-body">
              <h2>  {{$user->name}} </h2>
              <form class="form-horizontal" enctype="multipart/form-data" action="/user/{{$user->id}}/update" method="POST">
              {{method_field('PATCH')}}
              {{ csrf_field() }}
                <div class="form-group">
                  <label for="name">Firmenname</label>
                  <input type="text" class="form-control" name="name" placeholder="Firmenbezeichnung" value="{{$user->name}}">
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" class="form-control" name="email" placeholder="Email" value="{{$user->email}}">
                </div>
                <div class="form-group">
                  <label for="adress">Adresse</label>
                  <input type="text" class="form-control" name="adress" placeholder="Adresse" value="{{$user->adress}}">
                </div>
                <div class="form-group">
                  <label for="Logo">Logo</label>
                  <input type="file" class="form-control" name="logo" placeholder="Logo">
                </div>
                <button type="submit" class="btn btn-primary">Bearbeiten</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>